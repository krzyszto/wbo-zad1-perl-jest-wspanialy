#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include "err.h"
#define ustaw(x, y, z) oceny[x - 'A'][y - 'A'] = z; oceny[y - 'A'][x - 'A'] = z
#define pobierz(x, y) oceny[x - 'A'][y - 'A']

int oceny[26][26] = {{0}};

void zainicjalizuj(int argc, char **argv)
{
	for (char a = 'A'; a <= 'Z'; ++a) {
		for (char b = 'A'; b <= 'Z'; ++b) {
			ustaw(a, b, 0);
		}
	}
	for (int i = 3; i < argc; ++i) {
		char a, b;
		int ile;
		sscanf(argv[i], "%c%c%i", &a, &b, &ile);
		ustaw(a, b, ile);
	}
}

float uliniowin(char *pierwszy, char* drugi)
{
	size_t rozmiar1 = strlen(pierwszy);
	size_t rozmiar2 = strlen(drugi);
	int **wyniki = malloc(rozmiar1 * sizeof(int*));
	for (size_t i = 0; i < rozmiar1; ++i) {
		wyniki[i] = malloc(rozmiar2 * sizeof(int));
		wyniki[i][0] = 0;
	}
	for (size_t i = 0; i < rozmiar2; ++i) {
		wyniki[0][i] = 0;
	}
	int max = 0;
	for (size_t x = 1; x < rozmiar1; ++x) {
		for (size_t y = 1; y < rozmiar2; ++y) {
			int tmp = wyniki[x - 1][y - 1] + pobierz(pierwszy[x], drugi[y]);
			wyniki[x][y] = tmp > 0 ? tmp : 0;
			max = max < tmp ? tmp : max;
		}
	}
	return max;
}

int main(int argc, char **argv)
{
	/* dwa pierwsze argumenty - fasty */
	/* kolejne argumenty - macierz w postaci */
	/* %c%c%i */
	/* for (int i = 0; i < argc; ++i) { */
	/* 	printf("%s\n", argv[i]); */
	/* } */
	if (argc < 30) {
		fatal("za malo argumentow, format to FASTA, FASTA, opis_macierzy");
	}
	zainicjalizuj(argc, argv);
	/* z fd 3 sie bedzie czytac, do fd4 pisac */
	char a[10];
	sprintf(a, "%f", uliniowin(argv[1], argv[2]));
	if (write(4, a, strlen(a)) <= 0) {
		syserr("nie udalo sie wypisac!");
	}
	return EXIT_SUCCESS;
}
