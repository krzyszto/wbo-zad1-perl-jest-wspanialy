#!/usr/bin/env perl
#perl 5
use strict;
use warnings;
use List::Util qw(max);         # max, nie?
use File::Slurp 'read_file';    # wczytywanie pliku
use Data::Dumper qw(Dumper);    # do debugu
use feature qw(say);            # takie print z nowa linia na koncu
# TODO - dodac lvalue
# TODO - skompresowac regexy
# TODO - podawac parametry przez referencje

my $PROGRAM_C = "./uliniowienie";
$SIG{CHLD} = 'IGNORE';
# liczy 10 + 10%
sub wylicz_ile_linii {
    my $ile_linii = () = $_[0] =~ /\n/g;
    if ($ile_linii > 10) {
        $ile_linii = 10 + ($ile_linii / 10);
    }
    return $ile_linii;
}

# po prostu reservoir sample, arg1 to dane, arg2 to ile chcemy probek
sub reservoir_sample {
    my $ile_linii = $_[1];
    my @wynik;
    my @linie = split /\n/, $_[0];
    my $numer_linii = 0;
    foreach (@linie) {
        if (++$numer_linii <= $ile_linii) {
            push @wynik, $_;
        } elsif (rand(1)  <= ($ile_linii / $numer_linii)) {
            $wynik[rand(@wynik)] = $_;
        }
    }
    return @wynik;
}

# bierze jeden dlugi napis z sekwencja w kazdej linii i daje losowe 10 + 10%
sub reservoir {
    my $ile_linii = wylicz_ile_linii $_[0];
    return reservoir_sample($_[0], $ile_linii);
}

# jako argument - tablica nazw plikow w formacie FASTA
# wynik - losowa probka 10 + 10% sekwencji bialek z kazdego pliku
sub sparsuj_fasty {
    my $text;
    my @wynik;
    foreach (@_) {
        $text = read_file $_;
        $text =~ s/\*//g;
        $text =~ s/>.*\n/>/g;
        $text =~ s/\n//g;
        $text =~ s/>+/\n/g;
        $text =~ s/^\n+//g;
        $text = $text . "\n";
        foreach (reservoir($text)) {
            push @wynik, $_;
        }
    }
    return @wynik;
}

# pierwsze 2 argumenty - napisy, trzeci argument - funkcja oceniajaca, bierze po charze
sub znajdz_najlepsze_uliniowienie {
    my @wyniki_uliniowien;
    for (my $i = 0; $i <= length($_[0]); $i++) {
        push @wyniki_uliniowien, [(0) x (length($_[1]) + 1)];
    }
    # wyniki uliniowien maja wymiary length($_[0]) x length($_[1]);
    my $maks = 0;
    for (my $x = 1; $x <= length($_[0]); $x++) {
        for (my $y = 1; $y <= length($_[1]); $y++) {
            $wyniki_uliniowien[$x][$y] = $_[2]->(substr($_[0], $x - 1, 1), substr($_[1], $y - 1, 1)) + $wyniki_uliniowien[$x - 1][$y - 1];
            $maks = max($wyniki_uliniowien[$x][$y], $maks);
        }
    }
    return $maks;
}

# bierze drugi i trzeci argument i daje jeden napis posortowany,
# przydaje sie w (do)daj_wartosc i nigdize wiecej. dlatego dziwne argumenty
# ale pokazuje fajna ceche jezyka: jak sie wola funkcje z & i bez argumentow,
# to wolana funkcja dostaje nasza tablice argumentow
sub posortuj {
    if ($_[2] gt $_[3]) {
        $_[2] . $_[3];
    } else {
        $_[3] . $_[2];
    }
}

# pierwszy argument - hasz
# drugi, trzeci argument - literki na klucz
sub podaj_wartosc {
    $_[0]{posortuj("", "", $_[1], $_[2])};
}

# pierwszy arugment - haszmapa, drugi - wartosc - pozostale dwa - klucze
sub dodaj_wartosc {
    my $klucz = &posortuj;
    if (exists $_[0]{$klucz}) {
        $_[0]{$klucz} += $_[1];
    } else {
        $_[0]{$klucz} = $_[1];
    }
}

# dostaje jako argument nazwe pliku
sub sparsuj_macierz_oceny_i_daj_funkcje {
    my %wynik;
    my $tab = read_file($_[0]);
    $tab =~ s/^#[^\n]*\n//mg;
    $tab =~ s/ +/ /g;
    $tab =~ s/ -?[[:digit:]]* *\n/\n/g;
    $tab =~ s/\* *//g;
    $tab =~ s/ *\n/\n/g;
    $tab =~ s/\n[[:alnum:] -]+\n$//g;
    $tab =~ s/^\W+//g;
    my @wiersze = split /\n/, $tab;
    my @kolumny = split / /, (shift @wiersze);
    my @wart;
    my $nazwa_wiersza;
    foreach (@wiersze) {
        @wart = split / /,  $_;
        $nazwa_wiersza = shift @wart;
        for (my $i = 0; $i < scalar @wart; $i++) {
            my $do_dodania = int $wart[$i];
            if ($nazwa_wiersza eq $kolumny[$i]) {
                $do_dodania *= 2;
            }
            dodaj_wartosc(\%wynik, $do_dodania, $nazwa_wiersza, $kolumny[$i]);
        }
    }
    foreach (keys %wynik) {
        $wynik{$_} = int($wynik{$_} / 2);
    }
    # my $funkcja_oceniajaca = sub {
    #     return podaj_wartosc(\%wynik, @_);
    # };
    # return $funkcja_oceniajaca;
    return \%wynik;
}

# pierwszy argument - plik z FASTA
# pozostale - pliki z macierzami
sub oblicz {
    die "oczekuje przynajmniej jednego pliku z FASTA i jednego z macierzami"
        unless scalar @_ > 1;
    my @fasty = sparsuj_fasty(shift @_);
    my %wyniki;
    pipe my $czytaj, my $pisz or die "nie udalo sie pajpa zrobic";
    foreach (@_) {
        my $nazwa_macierzy = $_;
        $wyniki{$_} = 0;
        # my $funkcja = sparsuj_macierz_oceny_i_daj_funkcje($_);
        my $macierz_napis = "";
        my $macierz = sparsuj_macierz_oceny_i_daj_funkcje($_);
        foreach (keys %$macierz) {
            $macierz_napis = $macierz_napis . $_ . $macierz->{$_} . " ";
        }
        my @macierz_tablica = split / /, $macierz_napis;
        foreach (@fasty) {
            my $pierwszy = $_;
            foreach (@fasty) {
                next unless $_ ne $pierwszy;
                fork || exec($PROGRAM_C, $pierwszy, $_, @macierz_tablica);
                sysread($czytaj, my $wynik_od_c, 10);
                $wyniki{$nazwa_macierzy} += $wynik_od_c;
                    # znajdz_najlepsze_uliniowienie($pierwszy, $_, $funkcja) /
                    # (length($pierwszy) + length($_));
            }
        }
        #$wyniki{$_} = int ($wyniki{$_});
    }
    return \%wyniki;
}

die "juz policzylismy " . (Dumper @ARGV) if -e $ARGV[0] . 'wynik';
die "plik nie istnieje" unless -e $ARGV[0];
$^F = 10;
my $wynik = oblicz(@ARGV);
open (my $fh, '>', $ARGV[0] . 'wynik') or
    die "nie moglem stworzyc pliku, ARGV to " . (Dumper @ARGV);
foreach my $klucz (sort keys %$wynik) {
    print $fh $klucz . " " . $wynik->{$klucz} . "\n";
}
close $fh;
