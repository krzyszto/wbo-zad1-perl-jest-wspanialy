#!/usr/bin/env perl
#perl 5
use strict;
use warnings;
use File::Slurp 'read_file';    # wczytywanie pliku
use Data::Dumper qw(Dumper);    # do debugu
use feature qw(say);            # takie print z nowa linia na koncu
use Getopt::Std;
use IO::Dir;

# pierwszy argument - nazwa katalogu
sub podaj_zawartosc_katalogu {
    tie my %dir, 'IO::Dir', $_[0];
    my @nazwy_plikow = keys %dir;
    my $index = 0;
    $index++ until $nazwy_plikow[$index] eq '.' || $nazwy_plikow[$index] eq '..';
    splice(@nazwy_plikow, $index, 1);
    $index++ until $nazwy_plikow[$index] eq '.' || $nazwy_plikow[$index] eq '..';
    splice(@nazwy_plikow, $index, 1);
    for (@nazwy_plikow) {
        s/^/$_[0]\//;
        s/\/\//\//;
    }
    return @nazwy_plikow
};

my $NIEWOLNICZY_SKRYPT = './parser.pl';
my %opcje;
my $FASTA_DIR_FLAG = 'f';
my $MATRIX_DIR_FLAG = 'd';
my $THREAD_FLAG = 'j';
getopt($FASTA_DIR_FLAG . $MATRIX_DIR_FLAG .$THREAD_FLAG, \%opcje);
die "podac fastadir, matrixdir i ile watkow (-t) albo nie licze"
    unless scalar(keys %opcje) == 3;
my $MAX_DZIECI = int($opcje{$THREAD_FLAG});
my @fasty = podaj_zawartosc_katalogu $opcje{$FASTA_DIR_FLAG};
@fasty = grep{!/wynik/} @fasty;
my @macierze = podaj_zawartosc_katalogu $opcje{$MATRIX_DIR_FLAG};
my $numer_fasty = 0;

my $dzieci = 0;
# while ($dzieci < $MAX_DZIECI && $numer_fasty < scalar @fasty) {
#     fork || exec($NIEWOLNICZY_SKRYPT, $fasty[$numer_fasty], @macierze);
# } continue {
#     $dzieci++;
#     $numer_fasty++;
# }

exit unless $numer_fasty < scalar @fasty;

my $dziecko = -1;
while (($dzieci < $MAX_DZIECI && $numer_fasty < scalar @fasty) ||
       ($dziecko = waitpid -1, 0) > 0) {
    if ($dziecko  > 0) {
        $dzieci--;
    }
    if ($numer_fasty < ($#fasty + 1)) {
        fork || exec($NIEWOLNICZY_SKRYPT, $fasty[$numer_fasty], @macierze);
        $dzieci++;
        $numer_fasty++;
        $dziecko = 0;
    }
} continue {
    # exit unless $numer_fasty < scalar @fasty;
    redo unless ($dzieci == $MAX_DZIECI || $numer_fasty == scalar @fasty);
}

my @fasty_wynik = podaj_zawartosc_katalogu $opcje{$FASTA_DIR_FLAG};
@fasty_wynik = grep {/wynik/} @fasty_wynik;

my %wynik;
my %lista_wynikow;
my $max_nazwa = "nie istnieje";
my $max_wartosc = "-inf";
foreach (@fasty_wynik) {
    my $text = read_file $_;
    my @punkty = split /\n/, $text;
    foreach (@punkty) {
        s/^.* (-?[[:digit:]]+(\.[[:digit:]]*)?)$/$1/g;
    }
    my @pliki = split /\n/, $text;
    foreach (@pliki) {
        s/(^.*) -?[[:digit:]]+(\.[[:digit:]]*)?$/$1/g;
    }

    for (my $i = 0; $i < scalar (@punkty); $i++)
    {
        if (exists $wynik{$pliki[$i]}) {
            # @{$lista_wynikow{klucz}} to wyciagniecie wartosci tablicy

            push @{$lista_wynikow{$pliki[$i]}}, $punkty[$i];
            $wynik{$pliki[$i]} += $punkty[$i];
        } else {
            $lista_wynikow{$pliki[$i]} = [$punkty[$i]];
            $wynik{$pliki[$i]} = $punkty[$i];
        }
    }
    while ((my $klucz, my $wartosc) = each %wynik) {
        if ($wartosc > $max_wartosc) {
            $max_wartosc = $wartosc;
            $max_nazwa = $klucz;
        }
    }
    # say (join(' ', @punkty));
}


say "Najlepsza macierza dopasowania jest $max_nazwa z uzyskanym wynikiem $max_wartosc";
foreach (keys %lista_wynikow) {
    say "$_ <- c(", join(', ', @{$lista_wynikow{$_}}), ")";
    say "wszystko <- c(", join(",", keys %lista_wynikow), ")";
    say "wynik <- matrix(wszystko, ncol=", scalar (keys %lista_wynikow), ")";
    say "boxplot(wynik)"
}
