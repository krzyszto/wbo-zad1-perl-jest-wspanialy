LIBS=err.o
LD=gcc
CC=gcc
CFLAGS=-std=gnu99 -Wall -Wextra -O2
LDFLAGS=-Wall -Wextra -O2

all: uliniowienie

uliniowienie: uliniowienie.o $(LIBS)
	$(LD) -o $@ $^ $(LDFLAGS)

clean:
	rm -f *.o *~ uliniowienie
